import { HuggingFaceInferenceEmbeddings } from "langchain/embeddings/hf";
import { RecursiveCharacterTextSplitter } from "langchain/text_splitter";
import { HuggingFaceInference } from "langchain/llms/hf";
import { loadQAStuffChain } from "langchain/chains";
import { Document } from "langchain/document";
import { timeout } from "./config";
import { OpenAIEmbeddings } from "langchain/embeddings/openai";


export const createPineconeIndex=async (client,indexName,vectorDimension)=>{
    console.log(`Checking ${indexName}...`)
    const existingIndexes=await client.listIndexes();
    if(!existingIndexes.includes(indexName)){
        console.log(`Creating index ${indexName}`)
        await client.createIndex({
            createRequest:{
                name:indexName,
                dimension: vectorDimension,
                metric: 'cosine',
            },
        });

        console.log(`creating index... please wait for finish init`);
        await new Promise((resolve)=>setTimeout(resolve,timeout));
    }else{
        console.log(`${indexName} already exists`);
    }
}

export const updatePinecone= async (client,indexName,docs)=>{
    const index=client.Index(indexName);
    console.log(`Pinecone index retrieved: ${indexName}`)

    for(const doc of docs){
        console.log(`Processing document: ${doc.metadata.source}`)
        const txtPath=doc.metadata.source;
        const text=doc.pageContent;

        const textSplitter=new RecursiveCharacterTextSplitter({
            chunkSize: 1000,
        });
        console.log('Splitting text into chunks')
        const chunks=await textSplitter.createDocuments([text]);
        console.log(`number of chunks: ${chunks.length}`)
        const embeddingsArrays=await new HuggingFaceInferenceEmbeddings().embedDocuments(
        chunks.map((chunk)=>chunk.pageContent.replace(/\n/g, " "))
        )

        console.log(`Creating ${chunks.length} vectors array with id, values and metadata...`);

        const batchSize=100;
        let batch:any=[];

        for(let idx=0;idx<chunks.length;idx++){
            const chunk=chunks[idx];
            const vector={
                id:`${txtPath}_${idx}`,
                values:embeddingsArrays[idx],
                metadata:{
                    ...chunk.metadata,
                    loc:JSON.stringify(chunk.metadata.loc),
                    pageContent: chunk.pageContent,
                    txtPath:txtPath
                },
            };
            batch=[...batch,vector]

            if(batch.length===batchSize ||idx===chunks.length-1){
                await index.upsert({
                    upsertRequest:{
                        vectors: batch,
                    },
                });
                batch=[];
            }
        }
    }
}


export const queryPineconeVectorStoreAndQueryLLM=async (client,indexName,question)=>{
    console.log('Querying pinecone vector store...')
    const index=client.Index(indexName);
    const queryEmbedding=await new HuggingFaceInferenceEmbeddings().embedQuery(question);
    let quesryResponse=await index.query({
        queryRequest:{
            topK:10,
            vector:queryEmbedding,
            includeMetadata:true,
            includeValues:true,
        }
    });

    console.log(`found ${quesryResponse.matches.length} matches...`)
    console.log(`Asking question: ${question}...`)

    if(quesryResponse.matches.length){
        const llm=new HuggingFaceInference({});
        const chain=loadQAStuffChain(llm);

        const concatenatedPageContent=quesryResponse.matches.map((match)=>match.metadata.pageContent).join(" ");
        const result=await chain.call({
            input_documents:[new Document({pageContent:concatenatedPageContent})],
            question:question
        })

        return result.text
    }else{
        console.log('No matches')
    }
}